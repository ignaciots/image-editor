package org.ignaciots.imageeditor.gui;

import java.awt.Color;

import org.ignaciots.imageeditor.editor.ImageEditor;

/**
 * Interface used when creating the app window.
 * @author Nacho T.
 * @version 0.1
 */

public interface EditorWindow {

	/**
	 * Updates the image from a editor
	 * @param editor the editor to be used
	 */
	void updateImage(ImageEditor editor);
	
	/**
	 * Loads an image from a file path
	 * @param fileName the file path
	 */
	void loadImage(String fileName);
	
	/**
	 * Saves the current image
	 */
	void saveImage();
	
	/**
	 * Saves the current image into a given path
	 * @param path The given path
	 */
	void saveImageAs(String path);
	
	/**
	 * Closes the current image
	 */
	void close();
	
	/**
	 * Increases the image resolution n times
	 * @param proportion The number of times to use when increasing the resolution
	 */
	void increase(int proportion);
	
	/**
	 * Decreases the image resolution n times
	 * @param proportion The number of times to use when decreasing the resolution
	 */
	void decrease(int proportion);
	
	/**
	 * Rotates the image to the right
	 */
	void rotateRight();
	
	/**
	 * Rotates the image to the left
	 */
	void rotateLeft();
	
	/**
	 * Inverts the image
	 */
	void rotateDown();
	
	/**
	 * Makes the current image brighter
	 */
	void bright();
	
	/**
	 * Makes the current image darker
	 */
	void dark();
	
	/**
	 * Colorizes the current image, merging its color with the new given color
	 * @param color The new color that will be merged with the image's color
	 */
	void color(Color color);
	
	/**
	 * Inverts the image color
	 */
	void negativeColor();
}
