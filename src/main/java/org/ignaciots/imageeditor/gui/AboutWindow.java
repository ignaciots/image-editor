package org.ignaciots.imageeditor.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.JTextPane;
import javax.swing.WindowConstants;
import javax.swing.JLabel;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.border.LineBorder;

import org.ignaciots.imageeditor.i18n.ResourceInternationalization;

import java.awt.event.ActionListener;
import java.net.URL;
import java.awt.event.ActionEvent;

/**
 * Class used for the About dialog
 * 
 * @author Nacho T.
 * @version 0.1
 */
public class AboutWindow extends JDialog {

	private static final long serialVersionUID = -6122131329793766296L;
	private MainAppWindow window;
	private final JPanel contentPanel = new JPanel();

	private JTextPane textPaneAbout;
	private JLabel lblAbout;
	private JPanel panelButton;
	private JButton btnAboutOk;

	public AboutWindow(MainAppWindow swingWindow) {
		this.window = swingWindow;
		loadIconImage();
		setTitle(window.getResourceInternationalization().getLocalizedString(ResourceInternationalization.ABOUT_TITLE));
		setResizable(false);
		setModal(true);
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		contentPanel.add(getTextPaneAbout(), BorderLayout.CENTER);
		contentPanel.add(getLblAbout(), BorderLayout.WEST);
		getContentPane().add(getPanelButton(), BorderLayout.SOUTH);
	}

	private JPanel getPanelButton() {
		if (panelButton == null) {
			panelButton = new JPanel();
			panelButton.setBorder(new LineBorder(SystemColor.controlHighlight, 2, true));
			FlowLayout flowLayout = (FlowLayout) panelButton.getLayout();
			flowLayout.setAlignment(FlowLayout.RIGHT);
			panelButton.add(getBtnAboutOk());
		}
		return panelButton;
	}

	private JLabel getLblAbout() {
		if (lblAbout == null) {
			lblAbout = new JLabel("");
			URL iconURL = ClassLoader.getSystemResource(MainAppWindow.iconPath);
			if (iconURL != null)
				lblAbout.setIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage(iconURL)));
		}
		return lblAbout;
	}

	private JTextPane getTextPaneAbout() {
		if (textPaneAbout == null) {
			textPaneAbout = new JTextPane();
			textPaneAbout.setForeground(SystemColor.windowText);
			textPaneAbout.setFont(new Font("Courier New", Font.PLAIN, 12));
			textPaneAbout.setText(this.window.getResourceInternationalization().getLocalizedString(ResourceInternationalization.ABOUT_TEXT_1) 
					+ "\n" + this.window.getResourceInternationalization().getLocalizedString(ResourceInternationalization.ABOUT_TEXT_VERSION) 
					+ "\n" + this.window.getResourceInternationalization().getLocalizedString(ResourceInternationalization.ABOUT_TEXT_AUTHOR)); 
			textPaneAbout.setBackground(SystemColor.controlHighlight);
		}
		return textPaneAbout;
	}

	private JButton getBtnAboutOk() {
		if (btnAboutOk == null) {
			btnAboutOk = new JButton("OK");
			btnAboutOk.addActionListener(new BtnAboutOkActionListener());
		}
		return btnAboutOk;
	}

	private void loadIconImage() {
		URL iconURL = ClassLoader.getSystemResource(MainAppWindow.iconPath);
		if (iconURL != null)
			setIconImage(Toolkit.getDefaultToolkit().getImage(iconURL));
	}

	private JDialog getThis() {
		return this;
	}

	private class BtnAboutOkActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			getThis().dispose();
		}
	}
}
