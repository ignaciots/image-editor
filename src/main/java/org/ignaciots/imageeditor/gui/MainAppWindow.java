package org.ignaciots.imageeditor.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.ignaciots.imageeditor.editor.DefaultImageEditor;
import org.ignaciots.imageeditor.editor.ImageEditor;
import org.ignaciots.imageeditor.exceptions.CorruptedImageException;
import org.ignaciots.imageeditor.i18n.ResourceInternationalization;

import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.InputEvent;
import javax.swing.JSeparator;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.border.BevelBorder;
import java.awt.FlowLayout;
import java.awt.Dimension;

/**
 * Simple Swing interface class
 * 
 * @author Nacho T.
 * @version 0.1 TODO Locale mnemonics TODO Paint TODO Fix revert
 */

public class MainAppWindow extends JFrame implements EditorWindow {

	private static final long serialVersionUID = 4909012017723126914L;
	private static final String defaultDirPath = System.getProperty("user.home") + "\\Pictures";
	static final String iconPath = "brush_icon.png";

	private ResourceInternationalization resourceI18n = new ResourceInternationalization();

	private ImageEditor editor = new DefaultImageEditor(this);
	private JFileChooser fileChooser;
	private JFileChooser saverChooser;

	private JPanel contentPane;
	private JPanel mainPanel;
	private JScrollPane imageScrollPane;
	private JLabel lblImage;
	private JMenuBar mainMenuBar;
	private JMenu mnFile;
	private JMenuItem mntmOpen;
	private JSeparator separatorExit;
	private JMenuItem mntmExit;
	private JMenu mnEdit;
	private JMenuItem mntmIncrease;
	private JMenuItem mntmDecrease;
	private JMenu mnHelp;
	private JMenuItem mntmHelp;
	private JMenuItem mntmAbout;
	private JSeparator separatorIncrease;
	private JMenuItem mntmRotateLeft;
	private JMenuItem mntmRotateRight;
	private JMenuItem mntmRotateDown;
	private JSeparator separatorRotator;
	private JMenuItem mntmColor;
	private JMenuItem mntmClose;
	private JLabel lblImgType;
	private JLabel lblImgName;
	private JMenuItem mntmBrighter;
	private JMenuItem mntmDarker;
	private JMenuItem mntmSave;
	private JMenuItem mntmSaveAs;
	private JLabel lblSaved;
	private JPanel statusPanel;
	private JSeparator separatorSaved;
	private JSeparator separatorImgType;
	private JSeparator separatorImgName;
	private JMenuItem mntmInvertirColor;

	public MainAppWindow() {
		loadApplicationIcon();
		setTitle(resourceI18n.getLocalizedString(ResourceInternationalization.APP_TITLE));
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 1024, 768);
		setJMenuBar(getMainMenuBar());
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		contentPane.add(getMainPanel(), BorderLayout.CENTER);
		addWindowListener(new WindowCloseEvent());
	}

	private void loadApplicationIcon() {
		URL iconURL = ClassLoader.getSystemResource(iconPath);
		if (iconURL != null)
			setIconImage(Toolkit.getDefaultToolkit().getImage(iconURL));
	}

	public ResourceInternationalization getResourceInternationalization() {
		return this.resourceI18n;
	}

	@Override
	public void updateImage(ImageEditor editor) {
		if (editor.getImage() == null) {
			getLblImage().setIcon(null);
			getLblImage().repaint();
			getLblSaved().setText(null);
		} else {
			getLblImage().setIcon(new ImageIcon(editor.getImage()));
			getLblImage().repaint();
			if (editor.isSaved()) {
				getLblSaved().setText(resourceI18n.getLocalizedString(ResourceInternationalization.SAVED_IMAGE_TEXT));
			} else
				getLblSaved()
						.setText(resourceI18n.getLocalizedString(ResourceInternationalization.NOT_SAVED_IMAGE_TEXT));
		}

	}

	@Override
	public void loadImage(String fileName) {
		try {
			editor.loadImage(fileName);
			getLblImgName().setText(editor.getFileName());
			String[] nameDivided = editor.getFileName().split("[.]");
			getLblImgType().setText(resourceI18n.getLocalizedString(ResourceInternationalization.IMAGE_TEXT_1) + " "
					+ nameDivided[nameDivided.length - 1].toUpperCase() + " "
					+ resourceI18n.getLocalizedString(ResourceInternationalization.IMAGE_TEXT_2));
			setEditingOptions(true);
			setSeparatorsStatus(true);
		} catch (IOException e) {
			showErrorDialog(resourceI18n.getLocalizedString(ResourceInternationalization.ERROR_IMAGE_TITLE),
					resourceI18n.getLocalizedString(ResourceInternationalization.ERROR_IO_TEXT));
		} catch (CorruptedImageException e) {
			showErrorDialog(resourceI18n.getLocalizedString(ResourceInternationalization.ERROR_IMAGE_TITLE),
					resourceI18n.getLocalizedString(ResourceInternationalization.ERROR_IMAGE_TEXT));
		}
	}

	@Override
	public void saveImage() {
		if (editor.getImage() != null) {
			try {
				editor.saveImage();
			} catch (IOException e) {
				showErrorDialog(resourceI18n.getLocalizedString(ResourceInternationalization.ERROR_IMAGE_TITLE),
						resourceI18n.getLocalizedString(ResourceInternationalization.ERROR_IO_TEXT));
			}
		}
	}

	@Override
	public void saveImageAs(String path) {
		if (editor.getImage() != null) {
			try {
				editor.saveImageAs(path);
			} catch (IOException e) {
				showErrorDialog(resourceI18n.getLocalizedString(ResourceInternationalization.ERROR_IMAGE_TITLE),
						resourceI18n.getLocalizedString(ResourceInternationalization.ERROR_IO_TEXT));
			}
		}
	}

	@Override
	public void close() {
		editor.closeImage();
		getLblImgName().setText(resourceI18n.getLocalizedString(ResourceInternationalization.NO_IMAGE_SELECTED));
		getLblImgType().setText("");
		setEditingOptions(false);
		setSeparatorsStatus(false);
	}

	@Override
	public void increase(int proportion) {
		if (editor.getImage() != null) {
			editor.increase(proportion);
		}
	}

	@Override
	public void decrease(int proportion) {
		if (editor.getImage() != null) {
			editor.decrease(proportion);
		}
	}

	@Override
	public void rotateLeft() {
		if (editor.getImage() != null) {
			editor.rotateLeft();
			;
		}
	}

	@Override
	public void rotateRight() {
		if (editor.getImage() != null) {
			editor.rotateRight();
			;
		}
	}

	@Override
	public void rotateDown() {
		if (editor.getImage() != null) {
			editor.rotateDown();
		}
	}

	@Override
	public void color(Color color) {
		if (editor.getImage() != null) {
			editor.mergeImageColor(color);
		}
	}

	@Override
	public void bright() {
		if (editor.getImage() != null)
			editor.bright();
	}

	@Override
	public void dark() {
		if (editor.getImage() != null)
			editor.dark();
	}
	
	@Override
	public void negativeColor() {
		if (editor.getImage() != null) {
			editor.negativeColor();
		}
	}

	private MainAppWindow getThis() {
		return this;
	}

	private void showErrorDialog(String title, String message) {
		JOptionPane.showMessageDialog(this, message, title, JOptionPane.ERROR_MESSAGE);
	}

	private String getUserInput(String title, String message) {
		return JOptionPane.showInputDialog(this, message, title, JOptionPane.QUESTION_MESSAGE);
	}

	private JFileChooser getImageChooser() {
		if (fileChooser == null) {
			fileChooser = new JFileChooser();
			fileChooser.addChoosableFileFilter(new FileNameExtensionFilter(
					resourceI18n.getLocalizedString(ResourceInternationalization.ALL_IMAGES_SELECETOR), "jpg", "jpeg",
					"bmp", "png", "gif"));
			fileChooser.addChoosableFileFilter(new FileNameExtensionFilter(
					resourceI18n.getLocalizedString(ResourceInternationalization.JPEG_SELECETOR), "jpg", "jpeg"));
			fileChooser.addChoosableFileFilter(new FileNameExtensionFilter(
					resourceI18n.getLocalizedString(ResourceInternationalization.PNG_SELECTOR), "png"));
			fileChooser.addChoosableFileFilter(new FileNameExtensionFilter(
					resourceI18n.getLocalizedString(ResourceInternationalization.BMP_SELECTOR), "bmp"));
			fileChooser.addChoosableFileFilter(new FileNameExtensionFilter(
					resourceI18n.getLocalizedString(ResourceInternationalization.ANIMATED_SELECTOR), "gif"));
			fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			fileChooser
					.setDialogTitle(resourceI18n.getLocalizedString(ResourceInternationalization.OPEN_IMAGE_CHOOSER));
			fileChooser.setAcceptAllFileFilterUsed(false);
			fileChooser.setMultiSelectionEnabled(false);
			fileChooser.setCurrentDirectory(new File(defaultDirPath));
		}
		return fileChooser;
	}

	private JFileChooser getSaverChooser() {
		if (saverChooser == null) {
			saverChooser = new JFileChooser();
			saverChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			saverChooser
					.setDialogTitle(resourceI18n.getLocalizedString(ResourceInternationalization.SAVE_IMAGE_CHOOSER));
			saverChooser.addChoosableFileFilter(new FileNameExtensionFilter(
					resourceI18n.getLocalizedString(ResourceInternationalization.JPEG_SELECETOR), "jpg", "jpeg"));
			saverChooser.addChoosableFileFilter(new FileNameExtensionFilter(
					resourceI18n.getLocalizedString(ResourceInternationalization.PNG_SELECTOR), "png"));
			saverChooser.addChoosableFileFilter(new FileNameExtensionFilter(
					resourceI18n.getLocalizedString(ResourceInternationalization.BMP_SELECTOR), "bmp"));
			saverChooser.addChoosableFileFilter(new FileNameExtensionFilter(
					resourceI18n.getLocalizedString(ResourceInternationalization.ANIMATED_SELECTOR), "gif"));
			saverChooser.setAcceptAllFileFilterUsed(false);
			saverChooser.setMultiSelectionEnabled(false);
			saverChooser.setCurrentDirectory(new File(defaultDirPath));
		}
		return saverChooser;
	}

	private void setEditingOptions(boolean enabled) {
		getMntmClose().setEnabled(enabled);
		getMntmSave().setEnabled(enabled);
		getMntmSaveAs().setEnabled(enabled);

		getMntmIncrease().setEnabled(enabled);
		getMntmDecrease().setEnabled(enabled);
		getMntmRotateLeft().setEnabled(enabled);
		getMntmRotateRight().setEnabled(enabled);
		getMntmRotateDown().setEnabled(enabled);
		getMntmBrighter().setEnabled(enabled);
		getMntmDarker().setEnabled(enabled);
		getMntmColor().setEnabled(enabled);
	}

	private void setSeparatorsStatus(boolean enabled) {
		getSeparatorSaved().setVisible(enabled);
		getLblSaved().setVisible(enabled);
		getLblImgType().setVisible(enabled);
		getSeparatorImgType().setVisible(enabled);
	}

	private JPanel getMainPanel() {
		if (mainPanel == null) {
			mainPanel = new JPanel();
			mainPanel.setBorder(null);
			mainPanel.setLayout(new BorderLayout(0, 0));
			mainPanel.add(getImageScrollPane());
			mainPanel.add(getStatusPanel(), BorderLayout.SOUTH);
		}
		return mainPanel;
	}

	private JScrollPane getImageScrollPane() {
		if (imageScrollPane == null) {
			imageScrollPane = new JScrollPane();
			imageScrollPane.setBorder(null);
			imageScrollPane.getVerticalScrollBar().setUnitIncrement(16);
			imageScrollPane.getHorizontalScrollBar().setUnitIncrement(16);
			imageScrollPane.setViewportView(getLblImage());
		}
		return imageScrollPane;
	}

	private JLabel getLblImage() {
		if (lblImage == null) {
			lblImage = new JLabel("");
			lblImage.setBorder(null);
			lblImage.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lblImage;
	}

	private JMenuBar getMainMenuBar() {
		if (mainMenuBar == null) {
			mainMenuBar = new JMenuBar();
			mainMenuBar.add(getMnFile());
			mainMenuBar.add(getMnEdit());
			mainMenuBar.add(getMnHelp());
		}
		return mainMenuBar;
	}

	private JMenu getMnFile() {
		if (mnFile == null) {
			mnFile = new JMenu(resourceI18n.getLocalizedString(ResourceInternationalization.MENU_FILE));
			mnFile.setMnemonic('F');
			mnFile.add(getMntmOpen());
			mnFile.add(getMntmSave());
			mnFile.add(getMntmSaveAs());
			mnFile.add(getMntmClose());
			mnFile.add(getSeparatorExit());
			mnFile.add(getMntmExit());
		}
		return mnFile;
	}

	private JMenuItem getMntmOpen() {
		if (mntmOpen == null) {
			mntmOpen = new JMenuItem(resourceI18n.getLocalizedString(ResourceInternationalization.MENU_ITEM_OPEN));
			mntmOpen.addActionListener(new MntmOpenActionListener());
			mntmOpen.setMnemonic('O');
			mntmOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_DOWN_MASK));
		}
		return mntmOpen;
	}

	private JSeparator getSeparatorExit() {
		if (separatorExit == null) {
			separatorExit = new JSeparator();
		}
		return separatorExit;
	}

	private JMenuItem getMntmExit() {
		if (mntmExit == null) {
			mntmExit = new JMenuItem(resourceI18n.getLocalizedString(ResourceInternationalization.MENU_ITEM_EXIT));
			mntmExit.addActionListener(new MntmExitActionListener());
			mntmExit.setMnemonic('E');
			mntmExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, InputEvent.ALT_DOWN_MASK));
		}
		return mntmExit;
	}

	private JMenu getMnEdit() {
		if (mnEdit == null) {
			mnEdit = new JMenu(resourceI18n.getLocalizedString(ResourceInternationalization.MENU_EDIT));
			mnEdit.setMnemonic('E');
			mnEdit.add(getMntmIncrease());
			mnEdit.add(getMntmDecrease());
			mnEdit.add(getSeparatorIncrease());
			mnEdit.add(getMntmRotateLeft());
			mnEdit.add(getMntmRotateRight());
			mnEdit.add(getMntmRotateDown());
			mnEdit.add(getSeparatorRotator());
			mnEdit.add(getMntmColor());
			mnEdit.add(getMntmBrighter());
			mnEdit.add(getMntmDarker());
			mnEdit.add(getMntmInvertirColor());
		}
		return mnEdit;
	}

	private JMenuItem getMntmIncrease() {
		if (mntmIncrease == null) {
			mntmIncrease = new JMenuItem(
					resourceI18n.getLocalizedString(ResourceInternationalization.MENU_ITEM_INCREASE));
			mntmIncrease.setEnabled(false);
			mntmIncrease.addActionListener(new MntmIncreaseActionListener());
			mntmIncrease.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_PLUS, InputEvent.CTRL_DOWN_MASK));
			mntmIncrease.setMnemonic('I');
		}
		return mntmIncrease;
	}

	private JMenuItem getMntmDecrease() {
		if (mntmDecrease == null) {
			mntmDecrease = new JMenuItem(
					resourceI18n.getLocalizedString(ResourceInternationalization.MENU_ITEM_DECREASE));
			mntmDecrease.setEnabled(false);
			mntmDecrease.addActionListener(new MntmDecreaseActionListener());
			mntmDecrease.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_MINUS, InputEvent.CTRL_DOWN_MASK));
			mntmDecrease.setMnemonic('D');
		}
		return mntmDecrease;
	}

	private JMenu getMnHelp() {
		if (mnHelp == null) {
			mnHelp = new JMenu(resourceI18n.getLocalizedString(ResourceInternationalization.MENU_HELP));
			mnHelp.setMnemonic('H');
			mnHelp.add(getMntmHelp());
			mnHelp.add(getMntmAbout());
		}
		return mnHelp;
	}

	private JMenuItem getMntmHelp() {
		if (mntmHelp == null) {
			mntmHelp = new JMenuItem(resourceI18n.getLocalizedString(ResourceInternationalization.MENU_HELP));
			mntmHelp.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
			mntmHelp.setMnemonic('e');
		}
		return mntmHelp;
	}

	private JMenuItem getMntmAbout() {
		if (mntmAbout == null) {
			mntmAbout = new JMenuItem(resourceI18n.getLocalizedString(ResourceInternationalization.MENU_ITEM_ABOUT));
			mntmAbout.addActionListener(new MntmAboutActionListener());
			mntmAbout.setMnemonic('t');
			mntmAbout.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F12, 0));
		}
		return mntmAbout;
	}

	private class WindowCloseEvent extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent we) {
			if (!getThis().editor.isSaved()) {
				int response = JOptionPane.showConfirmDialog(getThis(),
						resourceI18n.getLocalizedString(ResourceInternationalization.WARN_SAVE_TEXT),
						resourceI18n.getLocalizedString(ResourceInternationalization.WARN_SAVE_TITLE),
						JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (response == JOptionPane.YES_OPTION) {
					saveImage();
					getThis().setDefaultCloseOperation(EXIT_ON_CLOSE);
				} else if (response == JOptionPane.NO_OPTION) {
					getThis().setDefaultCloseOperation(EXIT_ON_CLOSE);
				} else if (response == JOptionPane.CANCEL_OPTION) {
					getThis().setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
				}
			} else {
				getThis().setDefaultCloseOperation(EXIT_ON_CLOSE);
			}
		}
	}

	private class MntmOpenActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			int result = getImageChooser().showOpenDialog(getThis());
			if (result == JFileChooser.APPROVE_OPTION) {
				loadImage(getImageChooser().getSelectedFile().getAbsolutePath());
			}
		}
	}

	private class MntmExitActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (!getThis().editor.isSaved()) {
				int response = JOptionPane.showConfirmDialog(getThis(),
						resourceI18n.getLocalizedString(ResourceInternationalization.WARN_SAVE_TEXT),
						resourceI18n.getLocalizedString(ResourceInternationalization.WARN_SAVE_TITLE),
						JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (response == JOptionPane.YES_OPTION) {
					saveImage();
					getThis().dispose();
				} else if (response == JOptionPane.NO_OPTION) {
					getThis().dispose();
				} else if (response == JOptionPane.CANCEL_OPTION) {
				}
			} else {
				getThis().dispose();
			}
		}
	}

	private class MntmIncreaseActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String response = getUserInput(
					resourceI18n.getLocalizedString(ResourceInternationalization.INPUT_DIALOG_TITLE),
					resourceI18n.getLocalizedString(ResourceInternationalization.INPUT_DIALOG_TEXT));
			if (response != null) {
				int number = 0;
				try {
					number = Integer.parseInt(response);
					increase(number);
				} catch (NumberFormatException exception) {
					showErrorDialog(resourceI18n.getLocalizedString(ResourceInternationalization.INPUT_ERROR_TITLE),
							resourceI18n.getLocalizedString(ResourceInternationalization.INPUT_ERROR_NUMBER_TEXT));
				} catch (IllegalArgumentException exception) {
					showErrorDialog(resourceI18n.getLocalizedString(ResourceInternationalization.INPUT_ERROR_TITLE),
							resourceI18n.getLocalizedString(ResourceInternationalization.INPUT_ERROR_BOUNDS_TEXT));
				}
			}
		}
	}

	private class MntmDecreaseActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String response = getUserInput(
					resourceI18n.getLocalizedString(ResourceInternationalization.INPUT_DIALOG_DECREASE_TITLE),
					resourceI18n.getLocalizedString(ResourceInternationalization.INPUT_DIALOG_DECREASE_TEXT));
			if (response != null) {
				int number = 0;
				try {
					number = Integer.parseInt(response);
					decrease(number);
				} catch (NumberFormatException exception) {
					showErrorDialog(resourceI18n.getLocalizedString(ResourceInternationalization.INPUT_ERROR_TITLE),
							resourceI18n.getLocalizedString(ResourceInternationalization.INPUT_ERROR_NUMBER_TEXT));
				} catch (IllegalArgumentException exception) {
					showErrorDialog(resourceI18n.getLocalizedString(ResourceInternationalization.INPUT_ERROR_TITLE),
							resourceI18n.getLocalizedString(ResourceInternationalization.INPUT_ERROR_BOUNDS_TEXT));
				}
			}
		}
	}

	private class MntmRotateLeftActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			rotateLeft();
		}
	}

	private class MntmRotateRightActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			rotateRight();
		}
	}

	private class MntmRotateDownActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			rotateDown();
		}
	}

	private class MntmColorActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			Color color = JColorChooser.showDialog(getThis(),
					resourceI18n.getLocalizedString(ResourceInternationalization.OPEN_COLOR_CHOOSER),
					new Color(0, 255, 0, 255));
			if (color != null)
				color(color);
		}
	}

	private class MntmCloseActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (!getThis().editor.isSaved()) {
				int response = JOptionPane.showConfirmDialog(getThis(),
						resourceI18n.getLocalizedString(ResourceInternationalization.WARN_SAVE_TEXT),
						resourceI18n.getLocalizedString(ResourceInternationalization.WARN_SAVE_TITLE),
						JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (response == JOptionPane.YES_OPTION) {
					saveImage();
					close();
				} else if (response == JOptionPane.NO_OPTION) {
					close();
				}
			}
			close();
		}

	}

	private class MntmBrighterActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			bright();
		}
	}

	private class MntmDarkerActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			dark();
		}
	}

	private class MntmSaveActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			saveImage();
		}
	}

	private class MntmSaveAsActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			int result = getSaverChooser().showSaveDialog(getThis());
			if (result == JFileChooser.APPROVE_OPTION) {
				File file = getSaverChooser().getSelectedFile();
				StringBuilder builder = new StringBuilder(file.getAbsolutePath());
				builder.append(".");
				builder.append(((FileNameExtensionFilter) getSaverChooser().getFileFilter()).getExtensions()[0]);
				saveImageAs(builder.toString());
			}

		}
	}

	private class MntmAboutActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			AboutWindow about = null;
			about = new AboutWindow(getThis());
			about.setVisible(true);
		}
	}
	private class MntmInvertirColorActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			negativeColor();
		}
	}

	private JSeparator getSeparatorIncrease() {
		if (separatorIncrease == null) {
			separatorIncrease = new JSeparator();
		}
		return separatorIncrease;
	}

	private JMenuItem getMntmRotateLeft() {
		if (mntmRotateLeft == null) {
			mntmRotateLeft = new JMenuItem(
					resourceI18n.getLocalizedString(ResourceInternationalization.MENU_ITEM_ROTATE_LEFT));
			mntmRotateLeft.setEnabled(false);
			mntmRotateLeft.addActionListener(new MntmRotateLeftActionListener());
			mntmRotateLeft.setMnemonic('l');
			mntmRotateLeft.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, InputEvent.CTRL_DOWN_MASK));
		}
		return mntmRotateLeft;
	}

	private JMenuItem getMntmRotateRight() {
		if (mntmRotateRight == null) {
			mntmRotateRight = new JMenuItem(
					resourceI18n.getLocalizedString(ResourceInternationalization.MENU_ITEM_ROTATE_RIGHT));
			mntmRotateRight.setEnabled(false);
			mntmRotateRight.addActionListener(new MntmRotateRightActionListener());
			mntmRotateRight.setMnemonic('r');
			mntmRotateRight.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_DOWN_MASK));
		}
		return mntmRotateRight;
	}

	private JMenuItem getMntmRotateDown() {
		if (mntmRotateDown == null) {
			mntmRotateDown = new JMenuItem(
					resourceI18n.getLocalizedString(ResourceInternationalization.MENU_ITEM_ROTATE_DOWN));
			mntmRotateDown.setEnabled(false);
			mntmRotateDown.addActionListener(new MntmRotateDownActionListener());
			mntmRotateDown.setMnemonic('w');
			mntmRotateDown.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_DOWN_MASK));
		}
		return mntmRotateDown;
	}

	private JSeparator getSeparatorRotator() {
		if (separatorRotator == null) {
			separatorRotator = new JSeparator();
		}
		return separatorRotator;
	}

	private JMenuItem getMntmColor() {
		if (mntmColor == null) {
			mntmColor = new JMenuItem(resourceI18n.getLocalizedString(ResourceInternationalization.MENU_ITEM_COLOR));
			mntmColor.setEnabled(false);
			mntmColor.addActionListener(new MntmColorActionListener());
			mntmColor.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_DOWN_MASK));
			mntmColor.setMnemonic('C');
		}
		return mntmColor;
	}

	private JMenuItem getMntmClose() {
		if (mntmClose == null) {
			mntmClose = new JMenuItem(resourceI18n.getLocalizedString(ResourceInternationalization.MENU_ITEM_CLOSE));
			mntmClose.setEnabled(false);
			mntmClose.addActionListener(new MntmCloseActionListener());
			mntmClose.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_DOWN_MASK));
			mntmClose.setMnemonic('C');
		}
		return mntmClose;
	}

	private JLabel getLblImgType() {
		if (lblImgType == null) {
			lblImgType = new JLabel();
			lblImgType.setVisible(false);
		}
		return lblImgType;
	}

	private JLabel getLblImgName() {
		if (lblImgName == null) {
			lblImgName = new JLabel(resourceI18n.getLocalizedString(ResourceInternationalization.NO_IMAGE_SELECTED));
		}
		return lblImgName;
	}

	private JMenuItem getMntmBrighter() {
		if (mntmBrighter == null) {
			mntmBrighter = new JMenuItem(
					resourceI18n.getLocalizedString(ResourceInternationalization.MENU_ITEM_BRIGHTER));
			mntmBrighter.setEnabled(false);
			mntmBrighter.setMnemonic('g');
			mntmBrighter.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, InputEvent.CTRL_DOWN_MASK));
			mntmBrighter.addActionListener(new MntmBrighterActionListener());
		}
		return mntmBrighter;
	}

	private JMenuItem getMntmDarker() {
		if (mntmDarker == null) {
			mntmDarker = new JMenuItem(resourceI18n.getLocalizedString(ResourceInternationalization.MENU_ITEM_DARKER));
			mntmDarker.setEnabled(false);
			mntmDarker.setMnemonic('k');
			mntmDarker.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_K, InputEvent.CTRL_DOWN_MASK));
			mntmDarker.addActionListener(new MntmDarkerActionListener());
		}
		return mntmDarker;
	}

	private JMenuItem getMntmSave() {
		if (mntmSave == null) {
			mntmSave = new JMenuItem(resourceI18n.getLocalizedString(ResourceInternationalization.MENU_ITEM_SAVE));
			mntmSave.setEnabled(false);
			mntmSave.addActionListener(new MntmSaveActionListener());
			mntmSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK));
			mntmSave.setMnemonic('S');
		}
		return mntmSave;
	}

	private JMenuItem getMntmSaveAs() {
		if (mntmSaveAs == null) {
			mntmSaveAs = new JMenuItem(resourceI18n.getLocalizedString(ResourceInternationalization.MENU_ITEM_SAVE_AS));
			mntmSaveAs.setEnabled(false);
			mntmSaveAs.addActionListener(new MntmSaveAsActionListener());
			mntmSaveAs.setMnemonic('a');
			mntmSaveAs
					.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK | InputEvent.ALT_DOWN_MASK));
		}
		return mntmSaveAs;
	}

	private JLabel getLblSaved() {
		if (lblSaved == null) {
			lblSaved = new JLabel();
			lblSaved.setVisible(false);
		}
		return lblSaved;
	}

	private JPanel getStatusPanel() {
		if (statusPanel == null) {
			statusPanel = new JPanel();
			FlowLayout flowLayout = (FlowLayout) statusPanel.getLayout();
			flowLayout.setAlignment(FlowLayout.TRAILING);
			flowLayout.setHgap(10);
			statusPanel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
			statusPanel.add(getSeparatorSaved());
			statusPanel.add(getLblSaved());
			statusPanel.add(getSeparatorImgType());
			statusPanel.add(getLblImgType());
			statusPanel.add(getSeparatorImgName());
			statusPanel.add(getLblImgName());
		}
		return statusPanel;
	}

	private JSeparator getSeparatorSaved() {
		if (separatorSaved == null) {
			separatorSaved = new JSeparator();
			separatorSaved.setVisible(false);
			separatorSaved.setPreferredSize(new Dimension(2, 20));
			separatorSaved.setOrientation(SwingConstants.VERTICAL);
		}
		return separatorSaved;
	}

	private JSeparator getSeparatorImgType() {
		if (separatorImgType == null) {
			separatorImgType = new JSeparator();
			separatorImgType.setVisible(false);
			separatorImgType.setOrientation(SwingConstants.VERTICAL);
			separatorImgType.setPreferredSize(new Dimension(2, 20));
		}
		return separatorImgType;
	}

	private JSeparator getSeparatorImgName() {
		if (separatorImgName == null) {
			separatorImgName = new JSeparator();
			separatorImgName.setOrientation(SwingConstants.VERTICAL);
			separatorImgName.setPreferredSize(new Dimension(2, 20));
		}
		return separatorImgName;
	}
	private JMenuItem getMntmInvertirColor() {
		if (mntmInvertirColor == null) {
			mntmInvertirColor = new JMenuItem("Invertir color");
			mntmInvertirColor.addActionListener(new MntmInvertirColorActionListener());
		}
		return mntmInvertirColor;
	}
}
