package org.ignaciots.imageeditor.i18n;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Class used to obtain localized resources such as {@link String}
 * based on a given {@link Locale}, or by default the current {@link Locale}.
 * @author Nacho Torre.
 * @version 0.1.1
 */
public class ResourceInternationalization {

	/*
	 * String localization keys
	 * TODO: Order this keys based on the .properties files.
	 */
	public static final String ABOUT_TEXT_1 = "aboutText1";
	public static final String ABOUT_TEXT_AUTHOR = "aboutTextAuthor";
	public static final String ABOUT_TEXT_VERSION = "aboutTextVersion"; 
	public static final String ABOUT_TITLE = "aboutTitle";
	public static final String ALL_IMAGES_SELECETOR = "allImagesSelector";
	public static final String ANIMATED_SELECTOR = "animatedSelector";
	public static final String APP_TITLE = "appTitle";
	public static final String BMP_SELECTOR = "bmpSelector";
	public static final String ERROR_IMAGE_TEXT = "errorImageText";
	public static final String ERROR_IMAGE_TITLE = "errorImageTitle";
	public static final String ERROR_IO_TEXT = "errorIOText";
	public static final String IMAGE_TEXT_1 = "imageText1";
	public static final String IMAGE_TEXT_2 = "imageText2";
	public static final String INPUT_DIALOG_DECREASE_TEXT = "inputDialogDecreaseText";
	public static final String INPUT_DIALOG_DECREASE_TITLE = "inputDialogDecreaseTitle";
	public static final String INPUT_DIALOG_TEXT = "inputDialogText";
	public static final String INPUT_DIALOG_TITLE = "inputDialogTitle";
	public static final String INPUT_ERROR_BOUNDS_TEXT = "inputErrorBoundsText";
	public static final String INPUT_ERROR_NUMBER_TEXT = "inputErrorNumberText";
	public static final String INPUT_ERROR_TITLE = "inputErrorTitle";
	public static final String JPEG_SELECETOR = "jpegSelector";
	public static final String MENU_EDIT = "menuEdit";
	public static final String MENU_FILE = "menuFile";
	public static final String MENU_HELP = "menuHelp";
	public static final String MENU_ITEM_ABOUT = "menuItemAbout";
	public static final String MENU_ITEM_BRIGHTER = "menuItemBrighter";
	public static final String MENU_ITEM_CLOSE = "menuItemClose";
	public static final String MENU_ITEM_COLOR = "menuItemColor";
	public static final String MENU_ITEM_DARKER = "menuItemDarker";
	public static final String MENU_ITEM_DECREASE = "menuItemDecrease";
	public static final String MENU_ITEM_EXIT = "menuItemExit";
	public static final String MENU_ITEM_INCREASE = "menuItemIncrease";
	public static final String MENU_ITEM_OPEN = "menuItemOpen";
	public static final String MENU_ITEM_ROTATE_DOWN = "menuItemRotateDown";
	public static final String MENU_ITEM_ROTATE_LEFT = "menuItemRotateLeft";
	public static final String MENU_ITEM_ROTATE_RIGHT = "menuItemRotateRight";
	public static final String MENU_ITEM_SAVE = "menuItemSave";
	public static final String MENU_ITEM_SAVE_AS = "menuItemSaveAs";
	public static final String MESSAGE_BUNDLE_NAME = "ImageEditor";
	public static final String NO_IMAGE_SELECTED = "noImageSelected";
	public static final String NOT_SAVED_IMAGE_TEXT = "notSavedImageText";
	public static final String OPEN_COLOR_CHOOSER = "openColorChooser";
	public static final String OPEN_IMAGE_CHOOSER = "openImageChooser";
	public static final String PNG_SELECTOR = "pngSelector";
	public static final String SAVE_IMAGE_CHOOSER = "saveImageChooser";
	public static final String SAVED_IMAGE_TEXT = "savedImageText";
	public static final String WARN_SAVE_TEXT = "warnSaveText";
	public static final String WARN_SAVE_TITLE = "warnSaveTitle";
	
	private final Locale currentLocale;
	private final ResourceBundle resourceBundle;
	
	
	/**
	 * Initializes a {@link ResourceInternationalization} based
	 * on the current {@link Locale} and using the {@link ResourceBundle}
	 * with the name MESSAGE_BUNDLE_NAME.
	 */
	public ResourceInternationalization() {
		this.currentLocale = Locale.getDefault(); 
		this.resourceBundle = ResourceBundle.getBundle(MESSAGE_BUNDLE_NAME, currentLocale);
		
	}
	
	/**
	 * Initializes a {@link ResourceInternationalization} with
	 * {@code locale} as the current {@link Locale} and using the {@link ResourceBundle}
	 * with the name {@code MESSAGE_BUNDLE_NAME}.
	 */
	public ResourceInternationalization(Locale locale) {
		this.currentLocale = locale;
		this.resourceBundle = ResourceBundle.getBundle(MESSAGE_BUNDLE_NAME, currentLocale);
	}
	
	/**
	 * Gets a localized {@link String} based on its
	 * key name. Key names can be found in the
	 * {@link ResourceInternationalization} class.
	 * @param key The {@link String} key of the localized resource.
	 * @return the localized {@link String} based on its key.
	 */
	public String getLocalizedString(String key) {
		return this.resourceBundle.getString(key);
	}
}
