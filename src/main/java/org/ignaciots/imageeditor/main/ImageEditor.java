package org.ignaciots.imageeditor.main;

import java.awt.EventQueue;

import org.ignaciots.imageeditor.gui.MainAppWindow;

/**
 * Main ImageEditor class.
 *
 * @author Nacho Torre.
 * @version 1.0.0
 */

public class ImageEditor {

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				MainAppWindow frame = new MainAppWindow();
				frame.setVisible(true);
			}
		});
	}
}
