package org.ignaciots.imageeditor.editor;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.ignaciots.imageeditor.exceptions.CorruptedImageException;
import org.ignaciots.imageeditor.gui.EditorWindow;

/**
 * Class used to edit a given {@link Image} and notify its changes to an {@link EditorWindow}.
 * @author Nacho Torre.
 * @version 0.1.1
 */

public class DefaultImageEditor implements ImageEditor {
	
	private String fileName;
	private Image image;
	private boolean saved;
	private EditorWindow window;
	
	/**
	 * Creates an ImageEditor with no window observer.
	 */
	public DefaultImageEditor() {
		this.window = null;
		this.saved = true;
	}
	
	/**
	 * Creates an ImageEditor with a given window observer.
	 * @param swingWindow the observer window.
	 */
	public DefaultImageEditor(EditorWindow swingWindow) {
		this();
		this.window = swingWindow;
	}
	
	/**
	 * Creates an ImageEditor with a window observer and a default image
	 * @param fileName the image's path
	 * @param window the observer window
	 * @throws IOException Thrown when the image cannot be loaded
	 * @throws CorruptedImageException Thrown when the loaded image is empty or corrupt
	 */
	public DefaultImageEditor(String fileName, EditorWindow window) throws IOException, CorruptedImageException {
		this(window);
		loadImage(fileName);
	}

	/* (non-Javadoc)
	 * @see org.nachotorre.imageeditor.editor.ImageEditor#getFileName()
	 */
	@Override
	public String getFileName() {
		return fileName;
	}
	

	/* (non-Javadoc)
	 * @see org.nachotorre.imageeditor.editor.ImageEditor#getImage()
	 */
	@Override
	public Image getImage() {
		return image;
	}
	
	/* (non-Javadoc)
	 * @see org.nachotorre.imageeditor.editor.ImageEditor#isSaved()
	 */
	@Override
	public boolean isSaved() {
		return this.saved;
	}
	
	/* (non-Javadoc)
	 * @see org.nachotorre.imageeditor.editor.ImageEditor#loadImage(java.lang.String)
	 */
	@Override
	public void loadImage(String fileName) throws IOException, CorruptedImageException {
		Image loadedImage = ImageIO.read(new File(fileName));
		if (loadedImage == null)
			throw new CorruptedImageException("The file " + fileName + "cannot be shown because it's empty or corrupt");
		this.image = loadedImage;
		this.fileName = fileName;
		notifyWindow();
	}
	
	/* (non-Javadoc)
	 * @see org.nachotorre.imageeditor.editor.ImageEditor#saveImage()
	 */
	@Override
	public void saveImage() throws IOException {
		saveImageAs(this.fileName);
	}
	
	/* (non-Javadoc)
	 * @see org.nachotorre.imageeditor.editor.ImageEditor#saveImageAs(java.lang.String)
	 */
	@Override
	public void saveImageAs(String path) throws IOException {
		if (this.image == null)
			throw new IllegalStateException("The current image cannot be null");
		String[] splitPath = path.split("[.]");
		ImageIO.write((RenderedImage) this.image, splitPath[splitPath.length - 1], new File(path));
		setSaved(true);
		notifyWindow();
	}
	
	/* (non-Javadoc)
	 * @see org.nachotorre.imageeditor.editor.ImageEditor#closeImage()
	 */
	@Override
	public void closeImage() {
		this.image = null;
		this.fileName = null;
		setSaved(true);
		notifyWindow();
	}
	
	/* (non-Javadoc)
	 * @see org.nachotorre.imageeditor.editor.ImageEditor#rotateRight()
	 */
	@Override
	public void rotateRight() {
		if (this.image == null)
			throw new IllegalStateException("Current image cannot be null");
		BufferedImage editImage = (BufferedImage) image;
		BufferedImage revertedImage = new BufferedImage(editImage.getHeight(), editImage.getWidth(), BufferedImage.TYPE_INT_ARGB);
		for (int i = 0; i < revertedImage.getWidth(); i++) {
			for (int j = 0; j < revertedImage.getHeight(); j++) {
				revertedImage.setRGB(i, j, editImage.getRGB(j, i));
			}
		}
		
		this.image = revertedImage;
		setSaved(false);
		notifyWindow();
	}
	
	/* (non-Javadoc)
	 * @see org.nachotorre.imageeditor.editor.ImageEditor#rotateLeft()
	 */
	@Override
	public void rotateLeft() {
		if (this.image == null)
			throw new IllegalStateException("Current image cannot be null");
		BufferedImage editImage = (BufferedImage) image;
		BufferedImage revertedImage = new BufferedImage(editImage.getHeight(), editImage.getWidth(), BufferedImage.TYPE_INT_ARGB);
		for (int i = revertedImage.getWidth() - 1; i >= 0; i--) {
			for (int j = revertedImage.getHeight() - 1; j >= 0; j--) {
				revertedImage.setRGB(i, j, editImage.getRGB(j, i));
			}
		}
		
		this.image = revertedImage;
		setSaved(false);
		notifyWindow();
	}
	
	/* (non-Javadoc)
	 * @see org.nachotorre.imageeditor.editor.ImageEditor#rotateDown()
	 */
	@Override
	public void rotateDown() {
		if (this.image == null)
			throw new IllegalStateException("Current image cannot be null");
		BufferedImage editImage = (BufferedImage) image;
		BufferedImage revertedImage = new BufferedImage(editImage.getWidth(), editImage.getHeight(), BufferedImage.TYPE_INT_ARGB);
		int posX = editImage.getWidth() - 1;
		int posY = editImage.getHeight() - 1;
		for (int i = 0; i < revertedImage.getWidth(); i++) {
			for (int j = 0; j < revertedImage.getHeight(); j++) {
				revertedImage.setRGB(i, j, editImage.getRGB(posX, posY));
				if (posY != 0) posY--;
			}
			posY = editImage.getHeight() - 1;
			if (posX != 0) posX--;
		}
		
		this.image = revertedImage;
		setSaved(false);
		notifyWindow();
	}
	
	/* (non-Javadoc)
	 * @see org.nachotorre.imageeditor.editor.ImageEditor#increase(int)
	 */
	@Override
	public void increase(int proportion) {
		if (this.image == null)
			throw new IllegalStateException("Current image cannot be null");
		if (proportion <= 0)
			throw new IllegalArgumentException("Proportion (" + proportion + ") must be >= 0");
		BufferedImage editImage = (BufferedImage) image;
		int scaledWidth = editImage.getWidth() *  proportion;
		int scaledHeight = editImage.getHeight() * proportion;
		BufferedImage escalatedImage = new BufferedImage(scaledWidth, scaledHeight, BufferedImage.TYPE_INT_ARGB);
		Graphics2D graphics = escalatedImage.createGraphics();
        graphics.drawImage(editImage, 0, 0, scaledWidth, scaledHeight, null);
        graphics.dispose();
		
		this.image = escalatedImage;
		setSaved(false);
		notifyWindow();
	}
	
	/* (non-Javadoc)
	 * @see org.nachotorre.imageeditor.editor.ImageEditor#decrease(int)
	 */
	@Override
	public void decrease(int proportion) {
		if (this.image == null)
			throw new IllegalStateException("Current image cannot be null");
		if (proportion <= 0)
			throw new IllegalArgumentException("Proportion (" + proportion + ") must be >= 0");
		BufferedImage editImage = (BufferedImage) image;
		int scaledWidth = editImage.getWidth() /  proportion;
		int scaledHeight = editImage.getHeight() / proportion;
		BufferedImage escalatedImage = new BufferedImage(scaledWidth, scaledHeight, BufferedImage.TYPE_INT_ARGB);
		Graphics2D graphics = escalatedImage.createGraphics();
        graphics.drawImage(editImage, 0, 0, scaledWidth, scaledHeight, null);
        graphics.dispose();
		
		this.image = escalatedImage;
		setSaved(false);
		notifyWindow();
	}
	
	/* (non-Javadoc)
	 * @see org.nachotorre.imageeditor.editor.ImageEditor#mergeImageColor(java.awt.Color)
	 */
	@Override
	public void mergeImageColor(Color color) {
		if (this.image == null)
			throw new IllegalStateException("Current image cannot be null");
		if (color == null)
			throw new IllegalArgumentException("Color must not be null");
		BufferedImage editImage = (BufferedImage) this.image;
		for (int i = 0; i < editImage.getWidth(); i++) {
			for (int j = 0; j < editImage.getHeight(); j++) {
				Color pixelColor = new Color(editImage.getRGB(i, j));
				Color mergedColor = mergeColors(pixelColor, color);
				editImage.setRGB(i, j, mergedColor.getRGB());
			}
		}
		
		this.image = editImage;
		setSaved(false);
		notifyWindow();
	}
	
	/* (non-Javadoc)
	 * @see org.nachotorre.imageeditor.editor.ImageEditor#bright()
	 */
	@Override
	public void bright() {
		if (this.image == null)
			throw new IllegalStateException("Current image cannot be null");
		BufferedImage editImage = (BufferedImage) this.image;
		for (int i = 0; i < editImage.getWidth(); i++) {
			for (int j = 0; j < editImage.getHeight(); j++) {
				Color pixelColor = new Color(editImage.getRGB(i, j));
				pixelColor = pixelColor.brighter();
				editImage.setRGB(i, j, pixelColor.getRGB());
			}
		}
		
		this.image = editImage;
		setSaved(false);
		notifyWindow();
		
	}
	
	/* (non-Javadoc)
	 * @see org.nachotorre.imageeditor.editor.ImageEditor#dark()
	 */
	@Override
	public void dark() {
		if (this.image == null)
			throw new IllegalStateException("Current image cannot be null");
		BufferedImage editImage = (BufferedImage) this.image;
		for (int i = 0; i < editImage.getWidth(); i++) {
			for (int j = 0; j < editImage.getHeight(); j++) {
				Color pixelColor = new Color(editImage.getRGB(i, j));
				pixelColor = pixelColor.darker();
				editImage.setRGB(i, j, pixelColor.getRGB());
			}
		}
		
		this.image = editImage;
		setSaved(false);
		notifyWindow();
		
	}
	
	private Color mergeColors(Color color1, Color color2) {
		int red = (color1.getRed() + color2.getRed()) / 2;
		int green = (color1.getGreen() + color2.getGreen()) / 2;
		int blue = (color1.getBlue() + color2.getBlue()) / 2;
		int alpha = (color1.getAlpha() + color2.getAlpha()) / 2;
		return new Color(red, green, blue, alpha);
	}
	
	/* (non-Javadoc)
	 * @see org.nachotorre.imageeditor.editor.ImageEditor#negativeColor()
	 */
	@Override
	public void negativeColor() {
		if (this.image == null)
			throw new IllegalStateException("Current image cannot be null");
		BufferedImage editImage = (BufferedImage) this.image;
		for (int i = 0; i < editImage.getWidth(); i++) {
			for (int j = 0; j < editImage.getHeight(); j++) {
				Color pixelColor = new Color(editImage.getRGB(i, j));
				pixelColor = new Color(~pixelColor.getRGB());
				editImage.setRGB(i, j, pixelColor.getRGB());
			}
		}
		
		this.image = editImage;
		setSaved(false);
		notifyWindow();
		
	}
	
	private void setSaved(boolean saved) {
		this.saved = saved;
	}
	
	private void notifyWindow() {
		if (window != null)
			window.updateImage(this);
	}

	

	

	
	
	

}
