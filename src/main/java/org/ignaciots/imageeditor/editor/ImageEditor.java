package org.ignaciots.imageeditor.editor;

import java.awt.Color;
import java.awt.Image;
import java.io.IOException;
import java.nio.file.Path;

import org.ignaciots.imageeditor.exceptions.CorruptedImageException;

/**
 * Interface that represents an {@link ImageEditor}.
 * An {@link ImageEditor} loads an image, modifies it, stores it and knows it state.
 * @author Nacho Torre.
 * @version 0.1.1
 *
 */
public interface ImageEditor {

	String getFileName();

	Image getImage();

	boolean isSaved();

	/**
	 * Loads an {@link Image} given its {@code filename}.
	 * @param filename The name of the loaded {@link Image}.
	 * @throws IOException Thrown when the file cannot be loaded.
	 * @throws CorruptedImageException Thrown when the loaded image is corrupted or is not a valid file.
	 */
	void loadImage(String filename) throws IOException, CorruptedImageException;

	/**
	 * Saves the current {@link Image} to secondary memory.
	 * @throws IOException Thrown when the {@link Image} cannot be saved.
	 */
	void saveImage() throws IOException;

	/**
	 * Saves the current {@link Image} in a different path of the filesystem.
	 * @param path {@link String} representing the {@link Path} where the image will be saved.
	 * @throws IOException Thrown when there is an error saving the {@link Image}.
	 */
	void saveImageAs(String path) throws IOException;

	/**
	 * Closes the current {@link Image}.
	 */
	void closeImage();

	/**
	 * Rotates the current {@link Image} to the right.
	 */
	void rotateRight();

	/**
	 * Rotates the current {@link Image} to the left.
	 */
	void rotateLeft();

	/**
	 * Rotates the current {@link Image} upside-down.
	 */
	void rotateDown();

	/**
	 * Increases the image resolution n times
	 * @param proportion Number of times to be increased
	 */
	void increase(int proportion);

	/**
	 * Increases the image resolution n times
	 * @param proportion Number of times to be increased
	 */
	void decrease(int proportion);

	/**
	 * Merges the image's color with a given color
	 * @param color Color to be merged with the image's color
	 */
	void mergeImageColor(Color color);

	/**
	 * Makes the current image brighter
	 */
	void bright();

	/**
	 * Makes the current image darker
	 */
	void dark();

	void negativeColor();

}