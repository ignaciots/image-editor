package org.ignaciots.imageeditor.exceptions;

/**
 * Exception that sould be throw when an image is loaded and cannot be visualized
 * @version 0.1
 */

public class CorruptedImageException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 780335965957147301L;
	
	public CorruptedImageException() {
		super();
	}
	
	public CorruptedImageException(String message) {
		super(message);
	}

}
